import { Module } from '@nestjs/common';
import { TypeJobService } from './type-job.service';
import { TypeJobController } from './type-job.controller';
import { PrismaService } from 'src/prisma.service';

@Module({
  controllers: [TypeJobController],
  providers: [TypeJobService,PrismaService],
})
export class TypeJobModule {}
