import { BadRequestException, Injectable } from '@nestjs/common';
import { CreateTypeJobDto } from './dto/create-type-job.dto';
import { UpdateTypeJobDto } from './dto/update-type-job.dto';
import { PrismaService } from 'src/prisma.service';

@Injectable()
export class TypeJobService {
  constructor(private readonly prisma:PrismaService){}
  create(data: any) {
    return this.prisma.loaiCongViec.create({data});
  }

  findAll() {
    return this.prisma.loaiCongViec.findMany({where:{exist:false}});
  }

  findOne(id: number) {
    return this.prisma.loaiCongViec.findUnique({where:{id,exist:false}});
  }

  update(id: number, data:any) {
    return this.prisma.loaiCongViec.update({where:{id},data});
  }

  async remove(id: number) {
    const checkId = await this.prisma.loaiCongViec.findUnique({where:{id}})
    if(!checkId) 
      throw new BadRequestException("No Found id!")
    return this.prisma.loaiCongViec.update({where:{id},data:{exist:true}});
  }
}
