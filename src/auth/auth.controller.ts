import { Body, Controller, Post, Req, Res, NotFoundException } from '@nestjs/common';
import { AuthService } from './auth.service';
import { Request, Response } from 'express'
import {ApiBody, ApiProperty, ApiTags} from '@nestjs/swagger'
import { CreateAuthDto, CreateDetailAuthDto } from './dto/create-detail-auth.dto';
@ApiTags("Auth")
@Controller('api/v1/auth')
export class AuthController {
  constructor(private readonly authService: AuthService) { }
  @Post("login")
  @ApiBody({type :CreateAuthDto})
  async login(@Body() body, @Res() response: Response, @Req() request: Request): Promise<any> {
      const result = await this.authService.login(body)
      return response.status(200).json({
        status: "200",
        message: "Successfully!",
        content: result
      })

  }
  @Post("sign-up")
  @ApiBody({type :CreateDetailAuthDto})
  async signUp(@Body() body, @Res() response: Response): Promise<any> {
      const result = await this.authService.signUp(body)
      return response.status(200).json({
        status: "200",
        message: "Successfully!",
        content: result
      })
   
  }

}
