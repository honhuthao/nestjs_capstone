import { BadRequestException, Injectable } from '@nestjs/common';
import { PrismaService } from 'src/prisma.service';

@Injectable()
export class DetailJobService {
  constructor(private readonly prisma:PrismaService){}
  create(data: any) {
    return this.prisma.chiTietLoaiCongViec.create({data});
  }

  findAll() {
    return this.prisma.chiTietLoaiCongViec.findMany({where:{exist:false}});
  }

  findOne(id: number) {
    return this.prisma.chiTietLoaiCongViec.findUnique({where:{id,exist:false}});
  }

  update(id: number,data:any) {
    return this.prisma.chiTietLoaiCongViec.update({
      where:{id,exist:false},
      data
    })
  }

  updateFile(id:number,file:any){
    return this.prisma.chiTietLoaiCongViec.update({where:{id,exist:false},data:{hinh_anh:file.path}})
  }  

  async remove(id: number) {
    const checkId = await this.prisma.chiTietLoaiCongViec.findFirst({where:{id,exist:false}})
    if(!checkId) 
      throw new BadRequestException("No Found id!")
    return this.prisma.chiTietLoaiCongViec.update({where:{id},data:{
      exist:true,
    }});
  }
}
