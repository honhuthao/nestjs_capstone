import { Controller, Get, Post, Body, Put, Param, Delete, Res, Req, UseGuards, UseInterceptors, UploadedFile } from '@nestjs/common';
import { DetailJobService } from './detail-job.service';
import { Request, Response } from 'express';
import { ApiTags, ApiBearerAuth, ApiBody, ApiConsumes } from '@nestjs/swagger';
import { JwtAuthGuard } from 'src/auth/auth.guard';
import { CreateDetailJobDto, FileUploadDto } from './dto/create-detail-job.dto';
import { FileInterceptor } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import { extname } from 'path';

@ApiBearerAuth()
@ApiTags('ChiTietCongViec')
@Controller('api/v1/chi-tiet-loai-cong-viec')
export class DetailJobController {
  constructor(private readonly detailJobService: DetailJobService) { }

  @Post()
  @ApiBody({ type: CreateDetailJobDto })
  async create(@Body() data: any, @Req() request: Request, @Res() response: Response): Promise<any> {
    
    const result =await this.detailJobService.create(data);
    return response.status(200).json({
      status: "200",
      message: "Successfully!",
      content: result
    })
  }

  @Get()
  async findAll(@Req() request: Request, @Res() response: Response): Promise<any> {
    const result = await this.detailJobService.findAll()
    return response.status(200).json({
      status: "200",
      message: "Successfully!",
      content: result
    })
  }

  @Get(':id')
  async findOne(@Param('id') id: string, @Req() request: Request, @Res() response: Response): Promise<any> {
    const result = await this.detailJobService.findOne(+id)
    return response.status(200).json({
      status: "200",
      message: "Successfully!",
      content: result
    })
  }

  @UseGuards(JwtAuthGuard)
  @ApiBody({ type: CreateDetailJobDto })
  @Put(':id')
  async update(@Param('id') id: string, @Body() data: any, @Res() response: Response): Promise<any> {
    await this.detailJobService.update(+id, data);
    return response.status(200).json({
      status: "200",
      message: "Successfully!"
    })
  }

  @Post('upload-hinh-nhom-loai-cong-viec/:id')
  @UseInterceptors(
    FileInterceptor('file', {
      storage: diskStorage({
        destination: './uploads',
        filename: (req, file, callback) => {
          const uniqueSuffix =
            Date.now() + '-' + Math.round(Math.random() * 1e9);
          const ext = extname(file.originalname);
          const filename = `${uniqueSuffix}${ext}`;
          callback(null, filename);
        },
      }),
    }),
  )
  
  @ApiConsumes('multipart/form-data')
  @ApiBody({
    type: FileUploadDto,
  })
  async handleUpload(@UploadedFile() file: Express.Multer.File, @Res() response: Response, @Param('id') id: string) {
    await this.detailJobService.updateFile(+id, file)
    return response.status(200).json({
      status: "200",
      message: "Successfully!",
      content: file

    })
  }

  @UseGuards(JwtAuthGuard)
  @Delete(':id')
  async remove(@Param('id') id: string, @Req() request: Request, @Res() response: Response): Promise<any> {
    await this.detailJobService.remove(+id);
    return response.status(200).json({
      status: "200",
      message: "Successfully!",
    })
  }
}
