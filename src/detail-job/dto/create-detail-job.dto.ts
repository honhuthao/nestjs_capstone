import { ApiProperty } from "@nestjs/swagger"

export class CreateDetailJobDto {
    @ApiProperty()
    ten_chi_tiet: String
    @ApiProperty()
    hinh_anh: String
    @ApiProperty()
    ma_loai_cong_viec: Number
}
export class FileUploadDto {
    @ApiProperty({ type: 'string', format: 'binary' })
    file: any;
  }