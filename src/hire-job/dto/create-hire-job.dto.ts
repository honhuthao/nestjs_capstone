import { ApiProperty } from "@nestjs/swagger"

export class CreateHireJobDto {
    @ApiProperty()
    ma_cong_viec: Number
    @ApiProperty()
    ma_nguoi_thue: Number
    @ApiProperty()
    ngay_thue: Date
    @ApiProperty()
    hoan_thanh: Boolean
}
