import { Controller, Get, Post, Body, Patch, Param, Delete, Req, Res, Put, UseGuards } from '@nestjs/common';
import { HireJobService } from './hire-job.service';
import{Request,Response} from "express"
import {ThueCongViec } from '@prisma/client';
import { ApiTags ,ApiBearerAuth, ApiBody,} from '@nestjs/swagger';
import { JwtAuthGuard } from 'src/auth/auth.guard';
import { CreateHireJobDto } from './dto/create-hire-job.dto';

@ApiBearerAuth()
@ApiTags("ThueCongViec")
@Controller('api/v1/thue-cong-viec')
export class HireJobController {
  constructor(private readonly hireJobService: HireJobService) {}

  @ApiBody({type:CreateHireJobDto})
  @Post()
  async create(@Body() data:ThueCongViec ,@Req() request:Request ,@Res() response:Response):Promise<any> {
    const result =await this.hireJobService.create(data);
    return response.status(200).json({
      status:"200",
      message:"Successfully!",
      content:result
    })
  }

  @Get()
  async findAll(@Req() request:Request,@Res() response:Response):Promise<any> {
    const result = await this.hireJobService.findAll()
    return response.status(200).json({
      status:"200",
      message:"Successfully!",
      content:result
    })
  }
   @UseGuards(JwtAuthGuard)
   @Get('lay-danh-sach-da-thue')
   async findJobHired( @Req() request,@Res() response:Response):Promise<any>{
     const result = await this.hireJobService.findJobHired(request.user.id);
     return response.status(200).json({
       status:"200",
       message:"Successfully!",
       content:result
     })
   }

  @Get(':id')
  async findOne(@Param('id') id: string ,@Req() request:Request,@Res() response:Response):Promise<any> {
    const result =await this.hireJobService.findOne(Number(id));
    return response.status(200).json({
      status:"200",
      message:"Successfully!",
      content:result
    })
  }

 
 
  @Put('hoan-thanh-cong-viec/:id')
  async updateJobHired(@Param('id') id:string ,@Req() request,@Res() response:Response):Promise<any>{
    const result = await this.hireJobService.updateJobHired(+id);
    return response.status(200).json({
      status:"200",
      message:"Successfully!",
      content:result
    })
  }

  @ApiBody({type:CreateHireJobDto})
  @UseGuards(JwtAuthGuard)
  @Put(':id')
  async update(@Param('id') id: string, @Body() data: any,@Req() request:Request,@Res() response:Response):Promise<any> {
     const result =await this.hireJobService.update(+id, data);
    return response.status(200).json({
      status:"200",
      message:"Successfully!",
      content:result
    })
  }

  @UseGuards(JwtAuthGuard)
  @Delete(':id')
  async remove(@Param('id') id: string ,@Req() request:Request,@Res() response:Response):Promise<any> {
    await this.hireJobService.remove(+id);
    return response.status(200).json({
      status:"200",
      message:"Successfully!",
    })
  }
}
