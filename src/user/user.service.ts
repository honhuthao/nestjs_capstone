import { BadRequestException, ConflictException, Injectable } from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { PrismaService } from 'src/prisma.service';

@Injectable()
export class UserService {
  constructor(private prisma: PrismaService) { }
  async create(data: any) {
    const {email} = data
    const checkEmail = await this.prisma.nguoiDung.findFirst({where:{email,exist:false}})
    if  (checkEmail) {
      throw new ConflictException('Email already exists');
    }
    return this.prisma.nguoiDung.create({ data });
  }

  findAll() {
    return this.prisma.nguoiDung.findMany({ where: { exist: false }, include: { ThueCongViec: true } });
  }

  findOne(id: number) {
    return this.prisma.nguoiDung.findUnique({ where: { exist: false, id }, include: { ThueCongViec: true } })
  };

  searchUser(name: string) {
    return this.prisma.nguoiDung.findMany({
      where: {
        name: { contains: name },
        exist: false
      }
    })
  }

  async update(id: number, data: any) {
    const user =this.prisma.nguoiDung.findFirst({where: { id,exist:false }})
    return this.prisma.nguoiDung.update({ where: { id ,exist:false}, data:{...data,pass_word:(await user).pass_word} });
  }

  async remove(id: number) {
    const checkId = await this.prisma.nguoiDung.findUnique({where:{id}})
    if(!checkId) 
      throw new BadRequestException("No Found id!")
    return this.prisma.nguoiDung.update({ where: { id }, data: { exist: true } });
  }
}
