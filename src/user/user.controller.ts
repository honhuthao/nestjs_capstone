import { Controller, Get, Post, Body, Put, Param, Delete, Res, Req, UseGuards} from '@nestjs/common';
import { UserService } from './user.service';
import { Request, Response } from 'express';
import{NguoiDung} from "prisma/prisma-client"
import { ApiTags ,ApiBearerAuth, ApiBody,} from '@nestjs/swagger';
import { JwtAuthGuard } from 'src/auth/auth.guard';
import { CreateUserDto } from './dto/create-user.dto';


@ApiBearerAuth()
@ApiTags('User')
@Controller('api/v1/users')
export class UserController {
  constructor(private readonly userService: UserService) {}

  @Post()
  @ApiBody({type:CreateUserDto})
  async create(@Body() data: NguoiDung,@Req() request:Request,@Res() response:Response):Promise<any> {
    await this.userService.create(data);
    return response.status(200).json({
      status:"200",
      message:"Successfully!",
      content:""
    })
  }

  @Get()
  async findAll(@Req() request:Request,@Res() response:Response):Promise<any> {
    const result = await this.userService.findAll()
    return response.status(200).json({
      status:"200",
      message:"Successfully!",
      content:result
    })
  }

  @Get(':id')
  async findOne(@Param('id') id: string,@Req() request:Request,@Res() response:Response):Promise<any> {
    const result = await this.userService.findOne(+id)
    return response.status(200).json({
      status:"200",
      message:"Successfully!",
      content:result
    })
  }

  @Get('search/:name')
  async searchUser(@Param('name') name:string,@Res() resposne:Response):Promise<any>{
    const result  = await this.userService.searchUser(name)
    return resposne.status(200).json({
      status:'200',
      message:"Successfully!",
      content:result
    })
  }

  @UseGuards(JwtAuthGuard)
  @ApiBody({type:CreateUserDto})
  @Put(':id')
  async update(@Param('id') id: string, @Body() data: any,@Res() response:Response):Promise<any> {
    await this.userService.update(+id, data);
    return response.status(200).json({
      status:"200",
      message:"Successfully!"
    })
  }

  @Delete(':id')
  async remove(@Param('id') id: string ,@Req() request:Request,@Res() response:Response):Promise<any> {
    await this.userService.remove(+id);
    return response.status(200).json({
      status:"200",
      message:"Successfully!",
    })
  }
}
