import { BadRequestException, Injectable } from '@nestjs/common';
import { PrismaService } from 'src/prisma.service';
import {CongViec} from "prisma/prisma-client"

@Injectable()
export class JobService {
  constructor(private prisma: PrismaService) {}

  async create(data: CongViec) {
    return this.prisma.congViec.create({
      data,
    })
  }

  async findAll() {
    return this.prisma.congViec.findMany({where:{exist:false}})
  }

  async findOne(id: number) {
    return this.prisma.congViec.findUnique({where:{exist:false,
    id}})
  }


  async findTypeJob(){
    return this.prisma.congViec.findMany({where:{exist:false},
      include:{
        ChiTietLoaiCongViec:{
          include:{
            LoaiCongViec:true
          }
        }
      }
    })
  }  
  async findDetailTypeJobByIdDetailJob(id: number) {
    return this.prisma.chiTietLoaiCongViec.findMany({
      where: { exist: false,id },
      include: {
        CongViec: true,
      },
    });
  }
  async findDetailTypeJobByIdTypeJob(id: number) {
    return this.prisma.loaiCongViec.findMany({
      where: { exist: false,id },
      include: {
        ChiTietLoaiCongViec: true,
      },
    });
  }

  async findDetailTypeJobByIdJob(id: number) {
    return this.prisma.congViec.findMany({
      where: { exist: false,id },
      include:{
        NguoiDung:true,
        ChiTietLoaiCongViec:true,
      }
    });
    //done
  }

  async findDetailTypeJobByNameJob(name: string) {
    return this.prisma.congViec.findMany({
      where: { exist: false,ten_cong_viec: {contains:name} },
      include:{
        NguoiDung:true,
        ChiTietLoaiCongViec:true,
      }
    });
  }

  updateFile(id:number,file:any){
    console.log(file.path);
    
    return this.prisma.congViec.update({where:{id,exist:false},data:{hinh_anh:file.path}})
  }

  update(id: number,data:any) {
    return this.prisma.congViec.update({
      where:{id,exist:false},
      data
    })
  }

  async remove(id: number) {
    const checkId = await this.prisma.congViec.findUnique({where:{id}})
    if(!checkId) 
      throw new BadRequestException("No Found id!")
    return this.prisma.congViec.update({where:{id},data:{
      exist:true,
    }});
  }
}
