import { Controller, Get, Post, Body, Param, Delete, Req, Res, Put, UseGuards, UseInterceptors ,UploadedFile } from '@nestjs/common';
import { JobService } from './job.service';

import{Request,Response} from "express"
import { CongViec } from '@prisma/client';
import { ApiTags ,ApiBearerAuth, ApiBody, ApiConsumes,} from '@nestjs/swagger';
import { JwtAuthGuard } from 'src/auth/auth.guard';
import { CreateJobDto, FileUploadDto } from './dto/create-job.dto';
import { FileInterceptor } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import { extname } from 'path';


@ApiBearerAuth()
@ApiTags('CongViec')
@Controller('api/v1/cong-viec')
export class JobController {
  constructor(private readonly jobService: JobService) {}

  @ApiBody({type:CreateJobDto})
  @Post()
  async create(@Body() data: CongViec ,@Req() request:Request, @Res() response:Response ):Promise<any> {
    const result =await this.jobService.create(data)
    return response.status(200).json({
      status:"200",
      message:"Successfully!",
      content:result})   
  }

  @Get()
  async findAll(@Req() request:Request,@Res() response:Response):Promise<any> {
    const result = await this.jobService.findAll()
    return response.status(200).json({
      status:"200",
      message:"Successfully!",
      content:result
    })
  }

  @Get('lay-menu-loai-cong-viec')
  async findTypeJob (@Req() request:Request,@Res() response:Response):Promise<any>{
    const result  = await this.jobService.findTypeJob();
    return response.status(200).json({
      status:"200",
      message:"Successfully!",
      content:result
    })
  }

  @Get('lay-chi-tiet-loai-cong-viec/:id')
  async findDetailTypeJob( @Param('id') id: string ,@Req() request:Request,@Res() response:Response):Promise<any>{
    const result  = await this.jobService.findDetailTypeJobByIdTypeJob(Number(id));   
    return response.status(200).json({
      status:"200",
      message:"Successfully!",
      content:result
    })
  }

  @Get('lay-cong-viec-theo-chi-tiet-loai/:id')
  async findDetailTypeJobByIdDetailJo( @Param('id') id: string ,@Req() request:Request,@Res() response:Response):Promise<any>{
    const result  = await this.jobService.findDetailTypeJobByIdDetailJob(Number(id));
    return response.status(200).json({
      status:"200",
      message:"Successfully!",
      content:result
    })
  }

  @Get('lay-cong-viec-chi-tiet/:id')
  async findDetailTypeJobByID( @Param('id') id: string ,@Req() request:Request,@Res() response:Response):Promise<any>{
    const result  = await this.jobService.findDetailTypeJobByIdJob(Number(id));
    return response.status(200).json({
      status:"200",
      message:"Successfully!",
      content:result
    })
  }

  @Get('lay-danh-sach-cong-viec-theo-ten/:name')
  async findDetailTypeJobByName( @Param('name') name: string ,@Req() request:Request,@Res() response:Response):Promise<any>{
    const result  = await this.jobService.findDetailTypeJobByNameJob(name);
    return response.status(200).json({
      status:"200",
      message:"Successfully!",
      content:result
    })
  }

  @Get(':id')
  async findOne(@Param('id') id: string,@Req() request:Request,@Res() response:Response):Promise<any> {
    const result =  await this.jobService.findOne(Number(id));
    return response.status(200).json({
      status:"200",
      message:"Successfully!",
      content:result
    })
  }


  @UseGuards(JwtAuthGuard)
  @ApiBody({type:CreateJobDto})
  @Put(':id')
  async update(@Param('id') id: string, @Body() data:any ,@Req() request:Request,@Res() response:Response):Promise<any>  {
    const result = await this.jobService.update(+id,data);
    return response.status(200).json({
      status:"200",
      message:"Successfully!",
      content:result
    })
  }

  @Post('upload-hinh-cong-viec/:id')
  @UseInterceptors(
    FileInterceptor('file', {
      storage: diskStorage({
        destination: './uploads',
        filename: (req, file, callback) => {
          const uniqueSuffix =
            Date.now() + '-' + Math.round(Math.random() * 1e9);
          const ext = extname(file.originalname);
          const filename = `${uniqueSuffix}${ext}`;
          callback(null, filename);
        },
      }),
    }),
  )
  @ApiConsumes('multipart/form-data')
  @ApiBody({
    type: FileUploadDto,
  })
  async handleUpload(@UploadedFile() file: Express.Multer.File,@Res() response:Response,@Param('id') id: string,) {
    await this.jobService.updateFile(+id,file)
    return response.status(200).json({
      status:"200",
      message:"Successfully!"
    })
  }

  @UseGuards(JwtAuthGuard)
  @Delete(':id')
  async remove(@Param('id') id: string ,@Req() request:Request,@Res() response:Response):Promise<any> {
    await this.jobService.remove(+id);
    return response.status(200).json({
      status:"200",
      message:"Successfully!",
    })
  }
}
